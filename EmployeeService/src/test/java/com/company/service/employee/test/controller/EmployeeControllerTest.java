package com.company.service.employee.test.controller;

import com.company.service.employee.controller.EmployeeController;
import com.company.service.employee.exception.EmployeeAlreadyExistsException;
import com.company.service.employee.exception.EmployeeNotFoundException;
import com.company.service.employee.model.Employee;
import com.company.service.employee.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@WebMvcTest
public class EmployeeControllerTest {
	
	@Autowired
    private MockMvc mockMvc;
    private Employee employee;
    
    @MockBean
    private EmployeeService service;
    
    @InjectMocks
    private EmployeeController controller;
    private List<Employee> allEmployees = null;


    @SuppressWarnings("deprecation")
	@Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        employee = new Employee();

        employee.setID("5b04f7411764e3765c35f8f6");
        employee.setFirstName("Julio");
        employee.setMiddleInitial("Rene");
        employee.setLastName("Lopez");
        employee.setDateOfBirth(new Date(1985, 9, 16));
        employee.setDateOfEmployment(new Date());
        employee.setStatus(Employee.enumStatus.ACTIVE);

        allEmployees = new ArrayList<>();
        allEmployees.add(employee);


    }

    @Test
    public void createSuccess() throws Exception {

        when(service.create(any())).thenReturn(employee);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/employee")
                .contentType(MediaType.APPLICATION_JSON).content(asJsonString(employee)))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andDo(MockMvcResultHandlers.print());

    }

    @Test
    public void createFailure() throws Exception {

        when(service.create(any())).thenThrow(EmployeeAlreadyExistsException.class);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/employee")
                .contentType(MediaType.APPLICATION_JSON).content(asJsonString(employee)))
                .andExpect(MockMvcResultMatchers.status().isConflict())
                .andDo(MockMvcResultHandlers.print());

    }

    
    @Test
    public void deleteSuccess() throws Exception {

        when(service.delete("5b04f7411764e3765c35f8f6")).thenReturn(true);
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/employee/5b04f7411764e3765c35f8f6")
                .contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    
    @Test
    public void deleteFailure() throws Exception {

        when(service.delete("5b04f7411764e3765c35f8f6")).thenThrow(EmployeeNotFoundException.class);
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/employee/5b04f7411764e3765c35f8f6")
                .contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isNotFound())
                .andDo(MockMvcResultHandlers.print());
    }


    @Test
    public void updateSuccess() throws Exception {

        when(service.update(eq(employee.getID()), any())).thenReturn(employee);
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/employee/5b04f7411764e3765c35f8f6")
                .contentType(MediaType.APPLICATION_JSON).content(asJsonString(employee)))
                .andExpect(MockMvcResultMatchers.status().isOk()).andDo(MockMvcResultHandlers.print());
    }
   
    @Test
    public void updateFailure() throws Exception {

        when(service.update(eq(employee.getID()), any())).thenThrow(EmployeeNotFoundException.class);
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/employee/5b04f7411764e3765c35f8f6")
                .contentType(MediaType.APPLICATION_JSON).content(asJsonString(employee)))
                .andExpect(MockMvcResultMatchers.status().isNotFound()).andDo(MockMvcResultHandlers.print());
    }
    

    @Test
    public void getByIdSuccess() throws Exception {
        when(service.getById(employee.getID())).thenReturn(employee);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/employee/5b04f7411764e3765c35f8f6")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }


    @Test
    public void getCategoryByIdFailure() throws Exception {
        when(service.getById(employee.getID())).thenThrow(EmployeeNotFoundException.class);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/employee/5b04f7411764e3765c35f8f6")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }


    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
