package com.company.service.employee.aspectj;

import java.util.logging.Logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LoggingAspect {
	private static Logger logger = Logger.getLogger(LoggingAspect.class.getName());

	@Pointcut("execution(* com.company.service.employee.controller.EmployeeController.*(..))")
	public void logController() {
		// logController method
	}

	@Before("logController()")
	public void logBefore(JoinPoint joinPoint) {
		logger.info("Executing method @Before: " + joinPoint.getSignature().getName());
	}

	@After("logController()")
	public void logAfter(JoinPoint joinPoint) {
		logger.info("Executing method @After: " + joinPoint.getSignature().getName());
	}

	@AfterReturning("logController()")
	public void logAfterReturning(JoinPoint joinPoint) {
		logger.info("Executing method @AfterReturning: " + joinPoint.getSignature().getName());
	}

	@AfterThrowing("logController()")
	public void logAfterThrowing(JoinPoint joinPoint) {
		logger.info("Executing method @AfterThrowing: " + joinPoint.getSignature().getName());
	}
}
