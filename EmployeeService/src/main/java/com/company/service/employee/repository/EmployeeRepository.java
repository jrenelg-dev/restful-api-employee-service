package com.company.service.employee.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import com.company.service.employee.model.Employee;

@Repository
public interface EmployeeRepository extends MongoRepository<Employee, String> {
	
	@Query("{'Status': {$in: [?0]}}")
	List<Employee> findAllByStatus(String status);
}
