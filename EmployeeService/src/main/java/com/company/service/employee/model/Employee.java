package com.company.service.employee.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Employee {

	public static enum enumStatus {
		ACTIVE, INACTIVE
	}

	@Id
	private String ID;
	private String FirstName;
	private String MiddleInitial;
	private String LastName;
	private Date DateOfBirth;
	private Date DateOfEmployment;
	private enumStatus Status;
	
	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Employee(String iD, String firstName, String middleInitial, String lastName, Date dateOfBirth,
			Date dateOfEmployment, enumStatus status) {
		super();
		ID = iD;
		FirstName = firstName;
		MiddleInitial = middleInitial;
		LastName = lastName;
		DateOfBirth = dateOfBirth;
		DateOfEmployment = dateOfEmployment;
		Status = status;
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getMiddleInitial() {
		return MiddleInitial;
	}

	public void setMiddleInitial(String middleInitial) {
		MiddleInitial = middleInitial;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public Date getDateOfBirth() {
		return DateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		DateOfBirth = dateOfBirth;
	}

	public Date getDateOfEmployment() {
		return DateOfEmployment;
	}

	public void setDateOfEmployment(Date dateOfEmployment) {
		DateOfEmployment = dateOfEmployment;
	}

	public enumStatus getStatus() {
		return Status;
	}

	public void setStatus(enumStatus status) {
		Status = status;
	}

	@Override
	public String toString() {
		return "Employee [ID=" + ID + ", FirstName=" + FirstName + ", MiddleInitial=" + MiddleInitial + ", LastName="
				+ LastName + ", DateOfBirth=" + DateOfBirth + ", DateOfEmployment=" + DateOfEmployment + ", Status="
				+ Status + "]";
	}

}
