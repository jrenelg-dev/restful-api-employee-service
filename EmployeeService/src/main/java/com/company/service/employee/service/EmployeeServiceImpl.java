package com.company.service.employee.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.company.service.employee.exception.EmployeeAlreadyExistsException;
import com.company.service.employee.exception.EmployeeNotFoundException;
import com.company.service.employee.model.Employee;
import com.company.service.employee.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	private EmployeeRepository repository;

	@Autowired
	public EmployeeServiceImpl(EmployeeRepository repository) {
		super();
		this.repository = repository;
	} 
	
	@Override
	public Employee create(Employee employee) throws EmployeeAlreadyExistsException {
		Optional<Employee> optional = this.repository.findById(employee.getID());
		if (optional.isPresent()) {
			throw new EmployeeAlreadyExistsException();
		}
		employee.setDateOfEmployment(new Date());
		if (this.repository.insert(employee) == null) {
			throw new EmployeeAlreadyExistsException();
		}
		return employee;
	}

	@Override
	public Employee update(String id, Employee employee) throws EmployeeNotFoundException {
		Employee upateEmployee= null;
		Optional<Employee> optional = this.repository.findById(id);
		if(optional.isPresent()) {
			upateEmployee = optional.get();
			upateEmployee.setFirstName(employee.getFirstName());
			upateEmployee.setMiddleInitial(employee.getMiddleInitial());
			upateEmployee.setLastName(employee.getLastName());
			upateEmployee.setDateOfBirth(employee.getDateOfBirth());
			upateEmployee.setDateOfEmployment(employee.getDateOfEmployment());
			upateEmployee.setStatus(employee.getStatus());
			this.repository.save(upateEmployee);
		} else {
			throw new EmployeeNotFoundException();
		}
		return upateEmployee;
	}

	@Override
	public boolean delete(String id) throws EmployeeNotFoundException {
		boolean status = false;
		Optional<Employee> optional = this.repository.findById(id);
		if (optional.isPresent()) {
			this.repository.delete(optional.get());
			status = true;
		} else
			throw new EmployeeNotFoundException();
		return status;
	}

	@Override
	public Employee getById(String id) throws EmployeeNotFoundException {
		Optional<Employee> optional = this.repository.findById(id);
		if (optional.isPresent()) {
			return optional.get();
		} else
			throw new EmployeeNotFoundException();
	}

	@Override
	public List<Employee> getAll() {
		List<Employee> result = this.repository.findAll();
		if (!result.isEmpty()) {
			return result;
		} return new ArrayList<>();
	}
	
	@Override
	public List<Employee> getAllByStatus(String status) {
		return this.repository.findAllByStatus(status);
	}
}
