package com.company.service.employee.jwtfilter;

import org.springframework.web.filter.GenericFilterBean;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Date;

public class JwtFilter extends GenericFilterBean {
	
	private static final long EXPIRATION_TIME = 300000;
	
	
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpReq = (HttpServletRequest) request;
		HttpServletResponse httpRes = (HttpServletResponse) response;
		String authHeader = httpReq.getHeader("authorization");
		if(authHeader == null || !authHeader.startsWith("Bearer")) {
			throw new ServletException("Missing Invalid header auth");
		}
		
		String token = authHeader.substring(7);
		
		// the follow code is not required in this impl
		try {
			token = getToken("admin", "admin");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Follow lines will validate the token based in the secretKey
		Claims claims = Jwts.parser().setSigningKey("secretKey").parseClaimsJws(token).getBody();
		httpReq.setAttribute("claims", claims);
		chain.doFilter(request, response);
    }
    
    //This code will be in the auth api
	public String getToken(String username, String password) throws Exception {
		if(username == null || password == null) {
			throw new ServletException("Please provide proper username or password");
		}
		if(!username.equals("admin") || !password.equals("admin")) {
			throw new ServletException("Invalid credentials");
		}
		String jwtToken = Jwts.builder()
			.setSubject(username)
			.setIssuedAt(new Date())
			.setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
			.signWith(SignatureAlgorithm.HS256, "secretKey")
			.compact();
		return jwtToken;
	}
}
