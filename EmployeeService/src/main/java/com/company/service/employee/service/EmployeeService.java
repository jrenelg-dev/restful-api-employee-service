package com.company.service.employee.service;

import java.util.List;

import com.company.service.employee.exception.EmployeeAlreadyExistsException;
import com.company.service.employee.exception.EmployeeNotFoundException;
import com.company.service.employee.model.Employee;

public interface EmployeeService {
	Employee create(Employee employee) throws EmployeeAlreadyExistsException;
	Employee update(String id,Employee employee) throws EmployeeNotFoundException;
    boolean delete(String id) throws EmployeeNotFoundException;
    Employee getById(String id) throws EmployeeNotFoundException;
    List<Employee> getAll();
	List<Employee> getAllByStatus(String status);
}