package com.company.service.employee.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT, reason = "Employee Already Exists")
public class EmployeeAlreadyExistsException extends Exception {

	private static final long serialVersionUID = 1L;

	public EmployeeAlreadyExistsException() {
		super();
	}

	public EmployeeAlreadyExistsException(String message) {
		super(message);
	}

}
