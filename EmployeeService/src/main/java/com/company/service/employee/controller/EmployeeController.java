package com.company.service.employee.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.company.service.employee.exception.EmployeeAlreadyExistsException;
import com.company.service.employee.exception.EmployeeNotFoundException;
import com.company.service.employee.model.Employee;
import com.company.service.employee.service.EmployeeService;

@RestController
@RequestMapping("api/v1")
public class EmployeeController {

	@Autowired
	private EmployeeService service;

	public EmployeeController(EmployeeService service) {
		super();
		this.service = service;
	}
	
	@PostMapping({ "/employee"})
	public ResponseEntity<?> create(@RequestBody Employee employee) throws Exception {
		try {
			Employee obj = this.service.create(employee);
			return new ResponseEntity<Employee>(obj, HttpStatus.CREATED);
		} catch (EmployeeAlreadyExistsException e) {
			return new ResponseEntity<Employee>(HttpStatus.CONFLICT);
		}
	}

	@PutMapping("/employee/{id}")
	public ResponseEntity<?> update(@PathVariable("id") String id, @RequestBody Employee employee) throws Exception {
		try {
			this.service.update(id, employee);
		} catch (EmployeeNotFoundException e) {
			return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Employee>(employee, HttpStatus.OK);
	}
	
	@DeleteMapping("/employee/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") String id) throws Exception {
		try {
			this.service.delete(id);
		} catch (EmployeeNotFoundException e) {
			return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Employee>(HttpStatus.OK);
	}
	
	@GetMapping("/employee/{id}")
	public ResponseEntity<?> select(@PathVariable("id") String id) throws Exception {
		try {
			Employee employee = this.service.getById(id);
			return new ResponseEntity<Employee>(employee, HttpStatus.OK);
		} catch (EmployeeNotFoundException e) {
			return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping("/employee")
	public ResponseEntity<?> selectAll(@RequestParam(required = false) String status) throws Exception {
		List<Employee> employees = (status != null && status.length() > 0) ? this.service.getAllByStatus(status) : this.service.getAll();
		return new ResponseEntity<List<Employee>>(employees, HttpStatus.OK);
	}
	
}
